import React from 'react';
import Header from '../components/Header';
import Company from '../components/Company';
import Footer from '../components/Footer';

class CompanyPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      'logged': !!localStorage.getItem('token')
    }
  }

  render() {
    if (this.state.logged) {
      return (
          <div className="App">
            <Header/>
            <Company/>
            <Footer/>
          </div>
      );
    }
    else {
      window.location.replace('/login')
    }
  }
}

export default CompanyPage;
