import React from 'react';
import Header from '../components/Header';
import AfterEvent from '../components/AfterEvent';
import Footer from '../components/Footer';


class AfterEventPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      'logged': !!localStorage.getItem('token'),
    }
  }

  render() {
    if (this.state.logged) {
      return (
          <div className="App">
            <Header/>
            <AfterEvent name={this.props.name}/>
            <Footer/>
          </div>
      );
    }
    else {
      window.location.replace('/login')
    }
  }
}

export default AfterEventPage;