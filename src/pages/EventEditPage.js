import React from 'react';
import Header from '../components/Header';
import EventEdit from '../components/EventEdit';
import Footer from '../components/Footer';


class EventEditPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      'logged': !!localStorage.getItem('token'),
    }
  }

  render() {
    if (this.state.logged) {
      return (
          <div className="App">
            <Header/>
            <EventEdit name={this.props.name}/>
            <Footer/>
          </div>
      );
    }
    else {
      window.location.replace('/login')
    }
  }
}

export default EventEditPage;