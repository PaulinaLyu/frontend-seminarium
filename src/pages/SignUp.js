import React from 'react';
import Header from '../components/Header';
import FormSignUp from '../components/FormSignUp';
import Footer from '../components/Footer';

function SignUp() {
    return (
        <div>
            <Header/>
            <FormSignUp />
            <Footer/>
        </div>
    );
}

export default SignUp;
