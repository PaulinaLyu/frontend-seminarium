import React from 'react';
import Header from '../components/Header';
import Event from '../components/Event';
import Footer from '../components/Footer';

class EventPage extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      'logged': !!localStorage.getItem('token')
    }
  }

  render() {
    if (this.state.logged) {
      return (
          <div className="App">
            <Header/>
            <Event name={this.props.name}/>
            <Footer/>
          </div>
      )
    }
    else {
      window.location.replace('/login')
    }
  }
}

export default EventPage;