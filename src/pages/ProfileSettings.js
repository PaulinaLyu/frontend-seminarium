import React from 'react';
import Header from '../components/Header';
import Profile from '../components/Profile';
import Footer from '../components/Footer';


class ProfileSettings extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      'logged': !!localStorage.getItem('token'),
    }
  }


  render() {
    if (this.state.logged)
    return (
        <div>
          <Header/>
          <Profile/>
          <Footer/>
        </div>
    );
    else {
      window.location.replace('/login')
    }
  }
}

export default ProfileSettings;
