import React from 'react';
import Header from '../components/Header';
import User from '../components/User';
import Footer from '../components/Footer';

class UserPage extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      'logged': !!localStorage.getItem('token')
    }
  }

  render() {
    if (this.state.logged) {
      return (
          <div className="App">
            <Header/>
            <User/>
            <Footer/>
          </div>
      );
    }
    else {
      window.location.replace('/login')
    }
  }
}

export default UserPage;
