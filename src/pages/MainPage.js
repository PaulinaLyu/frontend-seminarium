import React from 'react';
import Header from '../components/Header';
import Footer from '../components/Footer';
import Intro from '../components/Intro';
import About from '../components/About';

function MainPage() {
    return (
        <div>
            <Header/>
            <Intro/>
            <About title = 'О продукте' text = 'Значимость этих проблем настолько очевидна, что постоянный количественный рост и сфера нашейактивности позволяет оценить значение системы обучения кадров, соответствует насущным потребностям.'/>
            <About title = 'Как пользоваться' text = 'Значимость этих проблем настолько очевидна, что постоянный количественный рост и сфера нашей активности позволяет оценить значение системы обучения кадров, соответствует насущным потребностям. Разнообразный и богатый опыт сложившаяся структура организации требуют от нас анализа форм развития.'/>
            <Footer/>
        </div>
    );
}

export default MainPage;
