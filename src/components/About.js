import React from 'react';
import BtnDetails from './BtnDetails';
import img from './../img/how_it_work.jpg';
import about from'./../scss/modules/about.module.scss';

function About(props) {
    return (
      	<div>
        	<div className = {about.about}>
         		<div className = 'container'>
            		<div className = 'row'>
              			<div className = 'col-lg-6'>
                			<h2 className = {about.title}>{props.title}</h2>
                			<p className = {about.text}>{props.text}</p>
                			<BtnDetails />
                			<div className = {about.alert}>
								<div className = {about.inner}>
									Ключевая суперважная информация
									бла бла бла бла бла бла бла бла бла бла бла бла бла 
								</div>
                			</div>
              			</div>
						<div className = 'col-lg-1'></div>
              			<div className = 'col-lg-5'>
                			<img className = {about.img} src= {img}  alt='О нас'/>
              			</div>
           			 </div>
         	 	</div>
          	</div>
      	</div>
    );
  }

  export default About;