import React from 'react';
import InputBootstrap from './InputBootstrap';
import ButtonOrange from './ButtonOrange';
// import ButtonOrangeLight from './ButtonOrangeLight';
import AccountQuestion from './AccountQuestion';
import formSignUp from '../scss/modules/formSignUp.module.scss';

class FormSignUp extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      'username': '',
      'email': '',
      'first_name': '',
      'last_name': '',
      'password': '',
    }
    this.handle_change = this.handle_change.bind(this)
    this.handle_signup = this.handle_signup.bind(this)
  }

  handle_change(e) {
    const fieldName = e.target.getAttribute('name')
    const fieldValue = e.target.value
    this.setState(prevstate => {
      const newState = {...prevstate};
      newState[fieldName] = fieldValue;
      return newState;
    });
  }

  handle_signup = (e) => {
    e.preventDefault();
    fetch('http://localhost:8000/api/users/', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(this.state)
    })
      .then(res => res.json())
      .then(json => {
        localStorage.setItem('token', json.token);
        window.location.replace('/user')
      });
  };

  render() {
    return (
        <div>
          <div className='container'>
            <div className={formSignUp.form}>
              <div className='card'>
                <div className={formSignUp.wrap}>
                  <div className={formSignUp.img}></div>
                  <div className={formSignUp.inner}>
                    <form onSubmit={this.handle_signup}>
                      <InputBootstrap change={this.handle_change} name='email' label='E-mail' type='email'
                                      placeholder='Введите ваш e-mail'/>
                      <InputBootstrap change={this.handle_change} name='username' label='Логин' type='text'
                                      placeholder='Введите ваш логин'/>
                      <InputBootstrap change={this.handle_change} name='first_name' label='Имя' type='text'
                                      placeholder='Введите ваше имя'/>
                      <InputBootstrap change={this.handle_change} name='last_name' label='Фамилия' type='text'
                                      placeholder='Введите вашу фамилию'/>
                      <InputBootstrap change={this.handle_change} name='password' label='Пароль' type='password'
                                      placeholder='Введите ваш пароль'/>
                      <InputBootstrap change={this.handle_change} name='repeat_password' label='Повторите пароль' type='password'
                                      placeholder='Введите ваш пароль'/>
                      <ButtonOrange type='submit' text='Начать'/>
                    </form>
                    {/*<ButtonOrangeLight text='Войти через Google'/>*/}
                    {/*<ButtonOrangeLight text='Войти через Facebook'/>*/}
                    <AccountQuestion explanation='Уже зарегистрированы?' link='Войти в аккаунт'/>
                  </div>

                </div>

              </div>
            </div>
          </div>
        </div>
    );
  }
}

export default FormSignUp;