import React from 'react';
// import icon from './../img/icon/right_arrow.png';
import button from './../scss/modules/buttons.module.scss';



function ButtonOrange(props) {
    return (
      	<div>
        	<button className={button.orange} type="submit" onClick={props.onclick}>
          		{props.text}
          		{/* <img  src= {icon} /> */}
        	</button>
      	</div>
    );
  }

  export default ButtonOrange;