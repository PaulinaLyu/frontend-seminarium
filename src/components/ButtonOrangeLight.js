import React from 'react';
import button from './../scss/modules/buttons.module.scss';
// import icon from './../img/icon/right_arrow.png';



function ButtonOrange(props) {
    return (
      	<div>
        	<button className = {button.orange_light} type="submit">
          		{props.text}
          		{/* <img  src= {icon} /> */}
        	</button>
      	</div>
    );
  }

  export default ButtonOrange;