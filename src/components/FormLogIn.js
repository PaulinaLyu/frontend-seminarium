import React from 'react';
import InputBootstrap from './InputBootstrap';
import ButtonOrange from './ButtonOrange';
import ButtonOrangeLight from './ButtonOrangeLight';
import AccountQuestion from './AccountQuestion';

class FormLogIn extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      'username': '',
      'password': '',
      'logged_fail': false,
    }
    this.handle_change = this.handle_change.bind(this)
    this.handle_login = this.handle_login.bind(this)
  }

  handle_change(e) {
    const fieldName = e.target.getAttribute('name')
    const fieldValue = e.target.value
    this.setState(prevstate => {
      const newState = {...prevstate};
      newState[fieldName] = fieldValue;
      return newState;
    });
  }

  handle_login = (e) => {
    e.preventDefault();
    fetch('http://localhost:8000/token-auth/', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(this.state)
    })
        .then(res => {
          if (res.status === 401) {
            this.setState(prevstate => {
            const newState = {...prevstate};
            newState['logged_fail'] = true;
            return newState;
    });
          }
        return res.json()
        })
        .then(json => {
          if ((typeof json.access !== 'undefined')) {
            localStorage.setItem('token', json.access);
            window.location.replace('/user')
          }
        })

  };

  render() {
    return (
        <div>
          <form onSubmit={this.handle_login}>
            <InputBootstrap name='username' change={this.handle_change} label='E-mail' type='e-mail'
                            placeholder='Введите ваш e-mail'/>
            <InputBootstrap name='password' change={this.handle_change} label='Пароль' type='password'
                            placeholder='Введите ваш пароль'/>
            {/*<div className="form-group form-check">*/}
            {/*<input type="checkbox" className="form-check-input" id="exampleCheck1"/>*/}
            {/*<label className="form-check-label" htmlFor="exampleCheck1">Запомните меня</label>*/}
            {/*</div>*/}
            <ButtonOrange text='Войти'/>
            {this.state.logged_fail ? (
                <h2>Пользователь не найден</h2>
            ) : null}
            {/*<ButtonOrangeLight text='Войти через Google'/>*/}
            {/*<ButtonOrangeLight text='Войти через Facebook'/>*/}
            <AccountQuestion href='/sign-up' explanation='Нет аккаунта?' link='Зарегистрироваться'/>
          </form>
        </div>
    );
  }
}

export default FormLogIn;