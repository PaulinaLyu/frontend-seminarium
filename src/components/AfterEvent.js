import React from 'react';
import event from './../scss/modules/event.module.scss';
import '../../node_modules/bootstrap/js/dist/dropdown.js';
import Rating from "./Rating"
import LeftPartEvent from "./LeftPartEvent";
import ButtonOrange from "./ButtonOrange";


class AfterEvent extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
        <div className={event.event}>
          <div className='container'>
            <div className='row'>
              <div className={event.shadow + ' ' + "col-lg-4"}>
                <LeftPartEvent/>
              </div>
              <div className={event.memberlist + " col-lg-8"}>
                <h3 className={event.h3}>Обратная связь</h3>
                <Rating rating={2} name='Общие впечатления'/>
                <Rating rating={2} name='Полезность материала'/>
                <Rating rating={2} name='Техническая оснащенность'/>
                <Rating rating={2} name='Еще что-то'/>
                <Rating rating={2} name='Еще что-то 2'/>
                <p>Дополнительные комментарии:</p>
                <textarea className="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                <div className={event.btn + ' row'}>
                  <div className='col-lg-4'><ButtonOrange text="Отправить" className={event.btn}/></div>
                  <div className='col-lg-8'></div>
                </div>
              </div>
            </div>
          </div>
        </div>
    );
  }
}

export default AfterEvent;