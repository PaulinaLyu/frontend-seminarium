import React from 'react';
import StarRatings from 'react-star-ratings';
import rating from '../scss/modules/rating.module.scss'

class Rating extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      rating: props.rating,
      name: props.name
    };
  }


  changeRating = (newRating, name) => {
    this.setState({
      rating: newRating
    });
  }

  render() {
    // rating = 2;
    return (
        <div className={rating.rating}>
          <p className={rating.name}>{this.state.name}</p>
          <StarRatings
              rating={this.state.rating}
              starRatedColor="#ED4D47"
              changeRating={this.changeRating}
              numberOfStars={5}
              name='rating'
              starDimension="30px"
              starSpacing="5px"
          />
        </div>
    );
  }
}

export default Rating;