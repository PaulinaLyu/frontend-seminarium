import React from 'react';
import FormLogIn from './FormLogIn';
import intro from './../scss/modules/intro.module.scss';
import card from './../scss/modules/card.module.scss';

function Intro() {
  return (
      <div>
        <div className={intro.intro}>
          <div className='container'>
            <div className='row'>
              <div className="col-lg-6">
                <div className={intro.title}>
                  <h1>Мероприятия <br/>в один клик</h1>
                </div>

              </div>
              <div className="col-lg-6">
                <div className={intro.form}>
                  <div className='card'>
                    <div className={card.inner}>
                      <FormLogIn/>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
  );
}

export default Intro;