import React from 'react';
import event from './../scss/modules/event.module.scss';
import member from './../scss/modules/member.module.scss'
import img from './../img/icon/men_blue.png';
import img4 from './../img/icon/person.png';
import img5 from './../img/icon/star.png';
import img6 from './../img/icon/person_blue.png';
import Member from "./Member";
import img1 from "../img/icon/calendar_blue.png";
import img2 from "../img/icon/pin_blue.png";
import img3 from "../img/icon/draw_blue.png";


import ButtonOrangeLight from "./ButtonOrangeLight";
import MemberList from "./MemberList";

const files = ['Файл 1.pdf', "Файл 2.pdf", "Файл 3.pdf"]
const filesList = files.map((name, index) => <p>{name}</p>)

const members = ['John Doe', 'John Doe', 'John Doe']
const membersList = members.map((name, index) => <Member key={index} text={name} email={name}/>)
const membersList1 = members.map((name, index) => <div key={index + 100} className='col-lg-6'><Member text={name}
                                                                                                      email={name}/>
</div>)


class EventEdit extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
        <div className={event.event}>
          <div className='container'>
            <div className='row'>
              <div className={event.shadow + ' ' + "col-lg-4"}>
                <div className={event.event}>
                  <h2 className={event.h2}>Настройка мероприятия</h2>
                  <div className={event.h2 + ' row'}>
                    <div className='col-lg-6'><h3>Описание</h3></div>
                    <div className='col-lg-6'><ButtonOrangeLight text='Удалить' /></div>
                  </div>
                  <p className={member.mrow}><img src={img} className={event.icon}/><input type="text"
                                                                                           className="form-control"
                                                                                           placeholder="Название семинара"
                                                                                           aria-describedby="basic-addon1"/>
                  </p>
                  <p className={member.mrow}><img src={img} className={event.icon}/>
                    <select id="inputState" className="form-control">
                      <option>Семинар</option>
                      <option>Хуинар</option>
                      <option>Въебинар</option>
                    </select>
                  </p>
                  <p className={member.mrow}><img src={img1} className={event.icon}/><input type="text"
                                                                                            className="form-control"
                                                                                            placeholder="09.11.18"
                                                                                            aria-describedby="basic-addon1"/>
                  </p>
                  <p className={member.mrow}><img src={img2} className={event.icon}/><input type="text"
                                                                                            className="form-control"
                                                                                            placeholder="ул. Кура Бура, д. 11, к. 2, каб. 20"
                                                                                            aria-describedby="basic-addon1"/>
                  </p>
                  <p className={member.mrow}>
                    <img src={img3} className={event.icon}/>
                    <textarea className="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                  </p>
                  <h3 className={event.h2}>Важные материалы</h3>
                  {filesList}
                </div>
              </div>
              <div className={event.memberlist + " col-lg-8"}>
                <MemberList/>
              </div>
            </div>
          </div>
        </div>
    );
  }
}

export default EventEdit;