import React from 'react';
import event from './../scss/modules/event.module.scss';
import member from './../scss/modules/member.module.scss'
import img from './../img/icon/men_blue.png';
import img4 from './../img/icon/person.png';
import img5 from './../img/icon/star.png';
import img6 from './../img/icon/person_blue.png';
import Member from "./Member";
import img1 from "../img/icon/calendar_blue.png";
import img2 from "../img/icon/pin_blue.png";
import img3 from "../img/icon/draw_blue.png";


import ButtonOrangeLight from "./ButtonOrangeLight";

const members = ['John Doe', 'John Doe', 'John Doe']
const membersList = members.map((name, index) => <Member key={index} text={name} email={name}/>)
const membersList1 = members.map((name, index) => <div key={index + 100} className='col-lg-6'><Member text={name}
                                                                                                      email={name}/>
</div>)


class MemberList extends React.Component {
  constructor(props) {
    super(props);
    this.handler = this.handler.bind(this)
    this.handler1 = this.handler1.bind(this)
    this.handler2 = this.handler2.bind(this)

    this.state = {
      author: ['John Doe1', 'John Doe2', 'John Doe3'],
      ruser: ['John Doe4', 'John Doe5', 'John Doe6'],
      users: ['John Doe7', 'John Doe8', 'John Doe9']
    };
  }

  handler(name) {
    let arr1 = this.state.ruser
    arr1.push(name);
    let arr2 = this.state.users
    for(var i = arr2.length - 1; i >= 0; i--) {
      if(arr2[i] === name) {
         arr2.splice(i, 1);
      }
    }
    this.setState({
      ruser: arr1,
      users: arr2
    })
  }

  handler1(name) {
    let arr1 = this.state.users
    arr1.push(name);
    let arr2 = this.state.ruser
    for(var i = arr2.length - 1; i >= 0; i--) {
      if(arr2[i] === name) {
         arr2.splice(i, 1);
      }
    }
    this.setState({
      ruser: arr2,
      users: arr1
    })
  }

  handler2(name) {

  let arr1 = this.state.users
  for(var i = arr1.length - 1; i >= 0; i--) {
    if(arr1[i] === name) {
       arr1.splice(i, 1);
    }
  }
  this.setState({
    users: arr1
  })
}


  render() {
    return (
        <div className="MemberList">
                <h3 className={event.h3}>Список участников</h3>
                <div className='row'>
                  <div className='col-lg-6'>
                    <p className={member.mrow}><img src={img4} className={event.icon + ' ' + event.info}/><b>Автор</b>
                    </p>
                    {this.state.author.map((name, index) => <Member key={index} text={name} email={name} handler={this.handler}/>)}
                  </div>
                  <div className='col-lg-6'>
                    <p className={member.mrow}><img src={img5}
                                                    className={event.icon + ' ' + event.info}/><b>Ответственные</b>
                    </p>
                    {this.state.ruser.map((name, index) => <Member key={index} text={name} email={name} handler={this.handler1} status='ruser'/>)}
                  </div>
                </div>

                <p className={member.mrow}><img src={img6} className={event.icon + ' ' + event.info}/><b>Участники</b>
                </p>
                <div className='row'>
                  {this.state.users.map((name, index) => <div key={index + 100} className='col-lg-6'><Member text={name}
                                                                                                      email={name} handler={this.handler} handler2={this.handler2} status='users'/>
</div>)}
                </div>
          </div>
    );
  }
}

export default MemberList;