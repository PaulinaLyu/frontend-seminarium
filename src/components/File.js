import React from "react";
import ButtonOrangeLight from "./ButtonOrangeLight";
import file from "../scss/modules/file.module.scss"

function File(props) {
    return (
        <div className={file.file}>
            <div className='container'>
                <div className='row'>
                    <div className='col-lg-7'>
                        <p>{props.text}</p>
                    </div>
                    <div className='col-lg-5'>
                        <ButtonOrangeLight text='Скачать' />
                    </div>
                </div>
            </div>
        </div>
    );
}

export default File;