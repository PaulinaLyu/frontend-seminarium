import React from "react";
import img from './../img/icon/avatar.png'
import star from'./../img/icon/star.png'
import cross from'./../img/icon/cross.png'
import member from '../scss/modules/member.module.scss'
import img4 from "../img/icon/person.png";
import event from "../scss/modules/event.module.scss";

class Member extends React.Component {
  render() {
    let handler = this.props.handler;
    let handler2 = this.props.handler2;
    return (

        <div className={member.member}>
          <div className='container'>
            <div className='row'>
              <div className={member.mrow + ' col-lg-3'}>
                <img src={img} className={member.icon}/>
              </div>
              <div className='col-lg-7'>
                <span className={member.name}>{this.props.text}</span><br/>
                <span className={member.mail}>{this.props.email}</span>
              </div>
              <div className='col-lg-2'>
                {this.props.status == 'ruser' &&
                  <img onClick={() => handler(this.props.text)} src={cross} className={member.image}/>
                }
                {this.props.status == 'users' &&
                    <div>
                      <img onClick={() => handler2(this.props.text)} src={cross} className={member.image}/><br/>
                      <img onClick={() => handler(this.props.text)} src={star} className={member.image}/>
                    </div>
                }
              </div>
            </div>
          </div>
        </div>
    );
  }
}

export default Member;