import React from 'react';
import input from './../scss/modules/input.module.scss';
import icon from './../img/icon/person_blue.png';

function InputModify(props) {
    return (
        <div className = {input.wrap}>
            <input type = {props.type} className = {input.input} placeholder = {props.placeholder}  />
            <label className = {input.label}>
                <img  className = {input.icon} src= {icon}  alt='Иконка'/>
            </label>
        </div>
    );
  }

  export default InputModify;