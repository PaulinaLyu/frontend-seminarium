import React from 'react';
import img1 from './../img/avatar.png';
import img2 from './../img/icon/men_blue.png';
import img3 from './../img/icon/mail_blue.png';
import img4 from './../img/icon/internet_blue.png';
import img5 from "../img/icon/draw_blue.png";
import Calendar from './Calendar'

class Company extends React.Component {

  constructor(props) {
    super(props);
      this.state = {
        company: '',
      }
      this.fetchTasks = this.fetchTasks.bind(this)
  }


  componentWillMount() {
    this.fetchTasks()
  }

  fetchTasks(){
    console.log('Fetching')
    fetch('http://127.0.0.1:8000/api/company/1/')
        .then(response => response.json())
        .then(data =>
            this.setState({
              company: data
            })
        )
  }

  render() {
    return (
        <div className>
          <div className='container'>
            <div className='row'>
              <div className="col-lg-4">
                <div className>
                  <h2 className>Страница компании</h2>
                  <h3 className>Описание</h3>
                  <img className='userpic' src={img1} alt='Аватар'/>
                  <p className><img src={img2} className='icon'/><b>{this.state.company.name}</b></p>
                  <p className><img src={img3} className='icon'/>{this.state.company.email}</p>
                  <p className><img src={img4} className='icon'/>{this.state.company.site}/</p>
                  <p><img src={img5} className='icon'/>Идейные соображения высшего порядка, а также
                    дальнейшее
                    развитие различных форм деятельности позволяет выполнять важные задания по
                    разработке дальнейших
                    направлений развития.</p>
                </div>
              </div>
            </div>
          </div>
          <div className='container'>
            <Calendar/>
          </div>
        </div>
    );
  }
}

export default Company;
