import React from 'react';
import event from './../scss/modules/event.module.scss';
import member from './../scss/modules/member.module.scss'
import img from './../img/icon/men_blue.png';
import img1 from "../img/icon/calendar_blue.png";
import img2 from "../img/icon/pin_blue.png";
import img3 from "../img/icon/draw_blue.png";

import File from '../components/File'

const files = ['Файл 1.pdf', "Файл 2.pdf", "Файл 3.pdf"]
const filesList = files.map((name, index) => <File key={index + 500} text={name}/>)


class LeftPartEvent extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
                <div className={event.event}>
                  <h2 className={event.h2}>Как эффективно работать</h2>
                  <h3 className={event.h2}>Описание</h3>
                  <p className={member.mrow}><img src={img} className={event.icon}/><b>Компания 1</b></p>
                  <p className={member.mrow}><img src={img} className={event.icon}/>Семинар</p>
                  <p className={member.mrow}><img src={img1} className={event.icon}/>09.11.18</p>
                  <p className={member.mrow}><img src={img2} className={event.icon}/>ул. Кура Бура, д. 11, к. 2, каб. 20
                  </p>
                  <p><img src={img3} className={event.icon}/>Идейные соображения высшего порядка, а также дальнейшее
                    развитие различных форм деятельности позволяет выполнять важные задания по разработке дальнейших
                    направлений развития.</p>
                  <h3 className={event.h2}>Важные материалы</h3>
                  {filesList}
                </div>
    );
  }
}

export default LeftPartEvent;