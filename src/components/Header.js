import React from 'react';
import ButtonOrange from './ButtonOrange';
import header from './../scss/modules/header.module.scss';
import logo from './../scss/modules/logo.module.scss';
import {Link} from 'react-router-dom'


class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      'logged': !!localStorage.getItem('token')
    }
  }

  handle_logout = () => {
    console.log(localStorage);
    localStorage.removeItem('token');
    this.setState({logged: false});
  };

  render() {
    if (!this.state.logged) {
      return (
          <div>
            <header className={header.header}>
              <div className='container'>
                <div className='row'>
                  <div className={header.inner}>
                    <div className="col-lg-2">
                      <a className={logo.logo} href='/'>
                        <span className={logo.prefix}>Meeting</span>Time
                      </a>
                    </div>
                    <div className="col-lg-8"></div>
                    <div className="col-lg-2">
                      <Link to="/login">
                      <ButtonOrange text='Войти'/>
                      </Link>
                    </div>
                  </div>
                </div>
              </div>
            </header>
          </div>
      );
    } else {
      return (
          <div>
            <header className={header.header}>
              <div className='container'>
                <div className='row'>
                  <div className={header.inner}>
                    <div className="col-lg-2">
                      <a className={logo.logo} href='/'>
                        <span className={logo.prefix}>Meeting</span>Time
                      </a>
                    </div>
                    <div className="col-lg-8"></div>
                    <div className="col-lg-2">
                      <Link to='/login'>
                        <ButtonOrange onclick={this.handle_logout} type='submit' text='Выйти'/>
                      </Link>
                    </div>
                  </div>
                </div>
              </div>
            </header>
          </div>
      );
    }
  }
}

export default Header;