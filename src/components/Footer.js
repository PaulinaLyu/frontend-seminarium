import React from 'react';
import footer from './../scss/modules/footer.module.scss';
import logo from './../scss/modules/logo.module.scss';

function Footer() {
  return (
      <div>
        <footer className={footer.footer}>
          <a className={logo.logo} href='#'>
            <span className={logo.prefix}>Meeting</span>Time
          </a>
          <p className={footer.copyright}>© Copyright. Все права защищены</p>
        </footer>
      </div>
  );
}

export default Footer;