import React from 'react';

function InputBootstrap(props) {
    return (
    <div className="form-group">
        <label htmlFor={props.id}>{props.label}</label>
        <input onChange={props.change} type={props.type} name={props.name} className='form-control form-control-lg' id={props.id}  placeholder={props.placeholder} />
    </div>
    );
  }

  export default InputBootstrap;