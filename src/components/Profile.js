import React from 'react';
import InputModify from './InputModify';
import ButtonOrange from './ButtonOrange';
import ButtonOrangeLight from './ButtonOrangeLight';
import profile from './../scss/modules/profile.module.scss';
import img from './../img/avatar.png';

function Profile(props) {
  return (
      <div className={profile.profile}>
        <div className='container'>
          <h2>Настройка профиля</h2>
          <div className='row'>
            <div className='col-lg-3'>
              <img className={profile.img} src={img} alt='Аватар'/>
              <InputModify type='file' placeholder='Загрузить'/>
            </div>
            <div className='col-lg-1'></div>
            <div className='col-lg-4'>
              <h2 className={profile.name}>John Doe</h2>
              <div className={profile.form}>
                <InputModify type='e-mail' placeholder='johnnydonny@gmail.com'/>
                <InputModify type='text' placeholder='John'/>
                <InputModify type='text' placeholder='Doe'/>
                <InputModify type='date' placeholder='08.08.1993'/>
              </div>
              <ButtonOrange text='Сохранить'/>
            </div>
            <div className='col-lg-1'></div>
            <div className='col-lg-3'>
              <h4 className={profile.password}>Изменить пароль</h4>
              <InputModify type='password' placeholder='Старый пароль'/>
              <InputModify type='password' placeholder='Новый пароль'/>
              <InputModify type='password' placeholder='Повторите новый пароль'/>
              <ButtonOrangeLight text='Сохранить'/>
            </div>
          </div>
        </div>
      </div>
  );
}

export default Profile;