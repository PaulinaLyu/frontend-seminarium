import React from 'react';
import icon from './../img/icon/right_arrow.png';
import btnDetails from './../scss/modules/btnDetails.module.scss';


function BtnDetails() {
    return (
      	<div>
        	<button className = {btnDetails.inner}>
          		<a className = {btnDetails.text} href='#'>Подробнее</a>
          		<img  src= {icon} />
        	</button>
      	</div>
    );
  }

  export default BtnDetails;