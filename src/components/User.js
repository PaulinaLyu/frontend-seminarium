import React from 'react';
import orange from './../scss/modules/buttons.module.scss';
import user from './../scss/modules/user.module.scss';
import img1 from './../img/icon/plus.png'
import Calendar from "./Calendar";


function User() {
  return (
      <div>
        <div>
          <div className='container'>
            <div className='row'>
              <div className="col-lg-2">
                <div className='row'>
                  <h3 className={user.h3style}>Список компаний</h3>
                  <button className={orange.orange_light}>Добавить <img src={img1} className={user.icon}/></button>
                  <div className={'overflow-auto ' + user.overflowstyle}>
                    <ul className="list-group">
                      <a href="#" className="list-group-item list-group-item-action">Компания 1</a>
                      <a href="#" className="list-group-item list-group-item-action">Компания 2</a>
                      <a href="#" className="list-group-item list-group-item-action">Компания 3</a>
                      <a href="#" className="list-group-item list-group-item-action">Компания 4</a>
                    </ul>
                  </div>
                  <h3 className={user.h3style}>Список мероприятий</h3>
                  <button className={orange.orange_light}>Добавить <img src={img1} className={user.icon}/></button>
                  <div className={'overflow-auto ' + user.overflowstyle}>
                    <ul className="list-group">
                      <a href="#" className="list-group-item list-group-item-action">Мероприятие 1</a>
                      <a href="#" className="list-group-item list-group-item-action">Мероприятие 2</a>
                      <a href="#" className="list-group-item list-group-item-action">Мероприятие 3</a>
                      <a href="#" className="list-group-item list-group-item-action">Мероприятие 4</a>
                    </ul>
                  </div>
                </div>
              </div>
              <div className="col-lg-1"></div>
              <div className="col-lg-9">
                <Calendar/>
              </div>
            </div>
          </div>
        </div>
      </div>
  );
}

export default User;

