import React from 'react';
import accountQuestion from './../scss/modules/accountQuestion.module.scss';

function AccountQuestion(props) {
    return (
        <div className = {accountQuestion.question}>
            <p className = {accountQuestion.explanation}>{props.explanation}</p>
            <a className = {accountQuestion.link} href={props.href}>{props.link}</a>
        </div>  
    );
}

  export default AccountQuestion;