import React from 'react';
import event from './../scss/modules/event.module.scss';
import member from './../scss/modules/member.module.scss'
import img from './../img/icon/men_blue.png';
import img4 from './../img/icon/person.png';
import img5 from './../img/icon/star.png';
import img6 from './../img/icon/person_blue.png';
import Member from "./Member";
import LeftPartEvent from "./LeftPartEvent";


const members = ['John Doe', 'John Doe', 'John Doe']
const membersList = members.map((name, index) => <Member key={index} text={name} email={name}/>)
const membersList1 = members.map((name, index) => <div key={index + 100} className='col-lg-6'><Member text={name}
                                                                                                      email={name}/>
</div>)


class Event extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
        <div className={event.event}>
          <div className='container'>
            <div className='row'>
              <div className={event.shadow + ' ' + "col-lg-4"}>
                <LeftPartEvent/>
              </div>
              <div className={event.memberlist + " col-lg-8"}>
                <h3 className={event.h3}>Список участников</h3>
                <div className='row'>
                  <div className='col-lg-6'>
                    <p className={member.mrow}><img src={img4} className={event.icon + ' ' + event.info}/><b>Автор</b>
                    </p>
                    {membersList}
                  </div>
                  <div className='col-lg-6'>
                    <p className={member.mrow}><img src={img5}
                                                    className={event.icon + ' ' + event.info}/><b>Ответственные</b>
                    </p>
                    {membersList}
                  </div>
                </div>
                <p className={member.mrow}><img src={img6} className={event.icon + ' ' + event.info}/><b>Участники</b>
                </p>
                <div className='row'>
                  {membersList1}
                </div>
              </div>
            </div>
          </div>
        </div>
    );
  }
}

export default Event;