import FullCalendar from '@fullcalendar/react'
import dayGridPlugin from '@fullcalendar/daygrid'
import timeGridPlugin from '@fullcalendar/timegrid'
import interactionPlugin from '@fullcalendar/interaction'
import bootstrapPlugin from '@fullcalendar/bootstrap';
import '../scss/overload-calendar.scss';
import React from "react";

function Calendar() {
  return (
      <FullCalendar
          defaultView="dayGridMonth"
          locale='ru'
          header={{
            left: 'title',
            center: '',
            right: 'prev,next'
          }}
          plugins={[
            dayGridPlugin,
            timeGridPlugin,
            interactionPlugin,
            bootstrapPlugin,
          ]}
          events={[
            {'title': 'Мероприятие 1', 'date': '2020-06-04'},
            {'title': 'Мероприятие 2', 'date': '2020-06-09'},
          ]}
          eventColor='#FACDC8'
          eventTextColor='#ED4D47'
          firstDay={1}
      />
  )
}

export default Calendar;