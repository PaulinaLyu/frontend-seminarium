import React from 'react';
import {BrowserRouter, Route} from "react-router-dom";
import MainPage from "./pages/MainPage";
import SignUp from "./pages/SignUp";
import ProfileSettings from './pages/ProfileSettings';
import CompanyPage from "./pages/CompanyPage";
import UserPage from "./pages/UserPage";
import EventPage from "./pages/EventPage";
import EventEditPage from "./pages/EventEditPage";
import AfterEventPage from "./pages/AfterEventPage";
import FormLogIn from "./components/FormLogIn";


class App extends React.Component {
  render() {
    return (
        <div className="App">
          <BrowserRouter>
            <Route exact path="/" component={MainPage}/>
            <Route exact path="/sign-up" component={SignUp}/>
            <Route exact path="/login" component={FormLogIn}/>
            <Route exact path="/profile-settings" component={ProfileSettings}/>
            <Route exact path="/company/:number" component={CompanyPage}/>
            <Route exact path="/user" component={UserPage}/>
            <Route exact path="/event" component={EventPage}/>
            <Route exact path="/event-edit" component={EventEditPage}/>
            <Route exact path="/after-event" component={AfterEventPage}/>
            {/*<Route exact path="/event" render={(props) => <EventPage {...props} name='default-event' />}/>*/}
            {/*<Route exact path="/event-edit" render={(props) => <EventPage {...props} name='edit-event' />}/>*/}
          </BrowserRouter>
        </div>
    );
  }
}

export default App;
